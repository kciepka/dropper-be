export class DbManager{

    constructor(url, dbName){
        this.url = "mongodb://" +  url + "/" + dbName;
        this.dbName = dbName;
        this.mongoClient = require('mongodb').MongoClient;
    }

    createCollection(colName){
        return new Promise((resolve, reject) => {
            this.mongoClient.connect(this.url, (err, db) => {
                if (err) reject(err);
                var dbase = db.db(this.dbName);
                dbase.createCollection(colName, (err, res) => {
                  if (err) reject(err);
                  db.close();
                  resolve(res);
                });
            });
        });
    }

    createIndex(colName, fieldName, options){
        return new Promise((resolve, reject) => {
            this.mongoClient.connect(this.url, (err, db) => {
                if (err) reject(err);
                var dbase = db.db(this.dbName);
                dbase.createIndex(colName, fieldName, options, (err, res) => {
                  if (err) reject(err);
                  db.close();
                  resolve(res);
                });
            });
        });
    }

    addDocument(colName, document){
        return new Promise((resolve, reject) => {
            this.mongoClient.connect(this.url, (err, db) => {
                if (err) reject(err);
                var dbase = db.db(this.dbName);
                dbase.collection(colName).insertOne(document, (err, res) => {
                  if (err) reject(err);
                  db.close();
                  resolve(res);
                });
            });
        });
    }

    getDocument(colName, query){
        return new Promise((resolve, reject) => {
            this.mongoClient.connect(this.url, (err, db) => {
                if (err) reject(err);
                var dbase = db.db(this.dbName);
                dbase.collection(colName).findOne(query, (err, res) => {
                  if (err) reject(err);
                  db.close();
                  resolve(res);
                });
            });
        });
    }

    getDocuments(colName, query){
        return new Promise((resolve, reject) => {
            this.mongoClient.connect(this.url, (err, db) => {
                if (err) reject(err);
                var dbase = db.db(this.dbName);
                dbase.collection(colName).find(query).toArray((err, res) => {
                  if (err) reject(err);
                  db.close();
                  resolve(res);
                });
            });
        });
    }

    updateDocument(colName, query, document){
        return new Promise((resolve, reject) => {
            this.mongoClient.connect(this.url, (err, db) => {
                if (err) reject(err);
                var dbase = db.db(this.dbName);
                dbase.collection(colName).replaceOne(query, document, (err, res) => {
                  if (err) reject(err);
                  db.close();
                  resolve(res);
                });
            });
        });
    }

    deleteDocument(colName, query){
        return new Promise((resolve, reject) => {
            this.mongoClient.connect(this.url, (err, db) => {
                if (err) reject(err);
                var dbase = db.db(this.dbName);
                dbase.collection(colName).deleteOne(query, (err, res) => {
                  if (err) reject(err);
                  db.close();
                  resolve(res);
                });
            });
        });
    }
}
