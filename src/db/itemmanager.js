import {DbManager} from './dbmanager.js';

export class ItemManager{

    constructor(url, dbName){
        this.db = new DbManager(url, dbName);
    }

    createItemsCollection(){
        this.db.createCollection("items")
        .then(() => {
            return this.db.createIndex("items", "id", { unique: true });
        })
        .then(() => {
            console.log("Items collection created");
        })
        .catch(function(err){
            console.log("Items collection not created correctly: ", err);
        });
    }

    addItem(item){
        return this.db.addDocument("items", item);
    }

    getItem(itemId){
        return this.db.getDocument("items", {id: itemId});
    }

    updateItem(item){
        return this.db.updateDocument("items", {id: item.id}, item);
    }

    deleteItem(itemId){
        return this.db.deleteDocument("items", {id: itemId});
    }
}