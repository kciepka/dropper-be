import {ItemManager} from './db/itemmanager.js';
const uuid = require('uuid/v4');
var express = require('express');
var items = require('./routes/items')

var app = express();
app.use('/items', items);
app.listen(3000, () => console.log('Example app listening on port 3000!'));

// var im = new ItemManager("localhost:27017", "dropperdb");

// im.createItemsCollection();

// console.log(uuid());
